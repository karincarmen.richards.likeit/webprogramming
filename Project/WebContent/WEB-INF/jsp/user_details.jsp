<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ詳細</title>
    <!-- 自分で作成したCSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="wrapper">
    <div class="header">
        <header>
            <nav>
                <a href="LogoutServlet">ログアウト</a>
                <span>${userInfo.name}さん</span>
            </nav>
        </header>
    </div>
    <div class="img">
    <div class="form-details">
    <div class="text-center">
    <h3>ユーザ情報詳細参照</h3>
    </div>
    <form method="post" action="UserDetailsServlet">
        <div class="text-center">
        <div class="form-group row">
            <label for="loginId" class="col-sm-6 col-form-label">ログインID</label>
                <div class="col-sm-2" >
                    <input disabled="disabled" type="text" class="form-control-plaint" id="loginId" name="loginId" value="${user.loginId}">
                </div>
        </div>
        <div class="form-group row">
            <label for="userName" class="col-sm-6 col-form-label">ユーザ名</label>
                <div class="col-sm-2">
                    <input disabled="disabled" type="text" class="form-control-plaint" id="userName" name="userName" value="${user.name}">
                </div>
        </div>
        <div class="form-group row">
            <label for="birthDay" class="col-sm-6 col-form-label">生年月日</label>
                <div class="col-sm-2" >
                    <input disabled="disabled" type="text" class="form-control-plaint" id="birthDate" name="birthDate" value="${user.birthDate}">
                </div>
        </div>
        <div class="form-group row">
            <label for="registerDate" class="col-sm-6 col-form-label">登録日時</label>
                <div class="col-sm-2" >
                    <input disabled="disabled" type="text" class="form-control-plaint" id="registerDate" name="registerDate" value="${user.createDate}">
                </div>
        </div>
        <div class="form-group row">
            <label for="updateDate" class="col-sm-6 col-form-label">更新日時</label>
                <div class="col-sm-2" >
                    <input disabled="disabled" type="text" class="form-control-plaint" id="updateDate" name="updateDate" value="${user.updateDate}">
                </div>
        </div>
        </div>
    </form>
    </div>
    <div class="retun_login">
        <a href="UserListServlet">戻る</a>
    </div>
    </div>
    <div class="footer">
        <footer>
            &copy;Example project.
        </footer>
    </div>

</body>
</html>