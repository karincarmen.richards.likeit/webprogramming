<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
    <!-- 自分で作成したCSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

</head>
		<c:if test = "${errMsg != null}" >
	    	<div class="alert alert-success" role="alert">
		 		${errMsg}
			</div>
		</c:if>

<body class="wrapper">
<div class="header">
        <header>
            <nav>
                <span>❖ようこそ❖</span>
            </nav>
        </header>
</div>
<div class="img">
<div class="form-signin">
        <h3>ログイン画面</h3>
    <form method="post" action="LoginServlet">
        <div class="form-group row">
            <label for="loginId" class="col-sm-6 col-form-label" >ログインID</label>
                <div class="col-sm-2" >
                    <input type="text" class="form-control-plaint" id="loginId" name="loginId" placeholder="ID">
                </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-6 col-form-label">パスワード</label>
                <div class="col-sm-2">
                    <input type="password" class="form-control-plaint" id="inputPassword" name="password"  placeholder="password">
                </div>
            <button type="submit" style="width: 100%" class="btn btn-outline-primary">ログイン</button>
        </div>
    </form>
</div>
</div>
    <div class="footer">
        <footer>
            &copy;Example project.
        </footer>
    </div>

</body>
</html>
