<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除</title>
<!-- 自分で作成したCSS -->
<link rel="stylesheet" href="css/style.css">
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="wrapper">
	<div class="header">
		<header>
			<nav>
				<a href="LogoutServlet">ログアウト</a> <span>${userInfo.name}さん</span>
			</nav>
		</header>
	</div>
	<div class="img">
		<div class="form-delete">
			<div class="text-center">
				<h3>ユーザ削除確認</h3>
			</div>
			<form method="post" action="UserDeleteServlet">
				<div class="text-center">
					<div class="form-group row">
						<label for="loginId" class="col-sm-6 col-form-label">ログインID</label>
						<div class="col-sm-2">
							<input readonly="readonly" type="text"
								class="form-control-plaint" id="loginId" name="loginId"
								value="${user.loginId}">
						</div>
					</div>
					<div class="form-group row">
						<label for="userName" class="col-sm-6 col-form-label">ユーザ名</label>
						<div class="col-sm-2">
							<input readonly="readonly" type="text"
								class="form-control-plaint" id="userName" name="userName"
								value="${user.name}">
						</div>
					</div>
					<p>を消します。本当に宜しいですか？</p>
					<div class="center">
						<button type="submit" style="width: 60%"
							class="btn btn-info btn-lg">OK</button>
					</div>
					<div class="center btn btn-danger" style="width: 60%">
						<a href="UserListServlet">キャンセル</a>
					</div>

				</div>
			</form>
		</div>
	</div>
	<div class="footer">
		<footer> &copy;Example project. </footer>
	</div>
</body>
</html>