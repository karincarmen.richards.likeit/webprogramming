<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ一覧</title>
    <!-- 自分で作成したCSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="wrapper">
    <div class="header">
        <header>
            <nav>
                <a href="LogoutServlet">ログアウト</a>
                <span>${userInfo.name}さん</span>
            </nav>
        </header>
      </div>
			<c:if test = "${errMsg != null}" >
	    	<div class="alert alert-warning" role="alert">
		  		${errMsg}
			</div>
			</c:if>
        <div>
    </div>
    <div class="img">
    <div class="form-list">
    <div class="text-center" >
        <h3 class="newuser">ユーザ一覧</h3>
    </div>
    <div class="new">
        <a href="UserRegistrationServlet">新規登録</a>
    </div>
    <form method="post" action="UserListServlet">
    <div class="text-center">
        <div class="form-group row">
            <label for="loginId" class="col-sm-6 col-form-label">ログインID</label>
                <div class="col-sm-2" >
                    <input type="text" name="loginId" class="form-control-plaint" id="loginId">
                </div>
        </div>
        <div class="form-group row">
            <label for="userName" class="col-sm-6 col-form-label">ユーザ名</label>
                <div class="col-sm-2" >
                    <input type="text" name="name" class="form-control-plaint" id="name">
                </div>
        </div>
        <div class="form-group row">
            <label for="birthDate" class="col-sm-6 col-form-label">生年月日</label>
            <div class="col-sm-6 text-center">
            </div>
                <div class="col-sm-12">
                      <input type="date" name="date-start" id="date-start" class="form-control" size="30"/>
                    </div>
                    <div class="col-xs-1 text-center">
                      ～
                    </div>
                    <div class="col-sm-12">
                      <input type="date" name="date-end" id="date-end" class="form-control"/>
                    </div>

        </div>
        <div class="center">
        <button type="submit" style="width: 60%" class="btn btn-primary btn-lg">検索</button>
        </div>
    </div>
    </form>
    </div>
    </div>
    <div class="user_table">
		<table>
			<thead>
            	<tr>
                	<th>ログインID</th>
                	<th>ユーザ名</th>
                	<th>生年月日</th>
                	<th></th>
            	</tr>
            </thead>
            <tbody>
            <tr>
            <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->

                     <td>
                       <a class="btn btn-primary" href="UserDetailsServlet?id=${user.id}">詳細</a>


                     	<c:if test="${userInfo.loginId == 'admin' || user.loginId == userInfo.loginId}">
                     		<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                     	</c:if>
 	                    <c:if test="${userInfo.loginId == 'admin'}">
 	                    <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
						</c:if>
                     </td>

                   </tr>
                 </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="footer">
        <footer>
            &copy;Example project.
        </footer>
    </div>

</body>
</html>