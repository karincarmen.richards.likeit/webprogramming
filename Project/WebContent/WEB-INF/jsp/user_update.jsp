<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ更新</title>
    <!-- 自分で作成したCSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body class="wrapper">
    <div class="header">
        <header>
            <nav>
                <a href="LogoutServlet">ログアウト</a>
                <span>${userInfo.name}さん</span>
            </nav>
        </header>
    </div>
    <c:if test = "${errMsg != null}" >
	    	<div class="alert alert-success" role="alert">
		 		${errMsg}
			</div>
	</c:if>
    <div class="img">
    <div class="form-update">
    <div class="text-center">
    <h3>ユーザ情報更新</h3>
    </div>
    <form method="post" action="UserUpdateServlet">
     <input type="hidden" class="form-control-plaint" id="id" name="id" value="${user.id}">

        <div class="text-center">
        <div class="form-group row">
            <label for="loginId" class="col-sm-6 col-form-label">ログインID</label>
                <div class="col-sm-2" >
                    <input readonly="readonly" type="text" class="form-control-plaint" id="loginId" name="loginId" value="${user.loginId}">
                </div>
        </div>
		<div class="form-group row">
            <label for="userName" class="col-sm-6 col-form-label">ユーザ名</label>
                <div class="col-sm-2" >
                    <input type="text" class="form-control-plaint" id="name" name="name" value="${user.name}">
                </div>
        </div>
        <div class="form-group row">
            <label for="birthDate" class="col-sm-6 col-form-label">生年月日</label>
                <div class="col-sm-2" >
                    <input type="text" class="form-control-plaint" id="birthDate" name="birthDate" value="${user.birthDate}">
                </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-6 col-form-label">パスワード</label>
                <div class="col-sm-2">
                    <input type="password" class="form-control-plaint" id="inputPassword" placeholder="password" name="password" value="${user.password}">
                </div>
        </div>
        <div class="form-group row">
            <label for="inputPassword" class="col-sm-6 col-form-label">パスワード確認</label>
                <div class="col-sm-2">
                    <input type="password" class="form-control-plaint" id="checkPassword" placeholder="password" name="checkPassword" value="${user.password}">
                </div>
        </div>
        <div class="center">
        <button type="submit" style="width: 60%" class="btn btn-primary btn-lg">更新</button>
        </div>
        </div>
    </form>
    </div>
    </div>
    <div class="retun_login">
        <a href="UserListServlet">戻る</a>
    </div>
    <div class="footer">
        <footer>
            &copy;Example project.
        </footer>
    </div>

</body>
</html>