package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {

			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//全てのユーザ情報を取得
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user EXCEPT WHERE id != 1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<User> findUserFromList(String loginId, String name, String dateStart, String dateEnd) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT*FROM user WHERE ";

			if (!loginId.equals("")) {
				sql += "login_id = '" + loginId + "'";

			}

			if (!name.equals("")) {
				sql += "name LIKE '%" + name + "%'";
			}

			if (!dateStart.equals("") || !dateEnd.equals("")) {
				sql += "birth_date >=  '" + dateStart + "' and birth_date <= '" + dateEnd+"'";
			}else if(!dateStart.equals("")) {
				sql += "birth_date >= '" + dateStart+"'";
			}else if (!dateEnd.equals("")) {
				sql += "birth_date <= " + dateEnd+"'";
			}


			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String nameDate = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				User user = new User(id, loginId1, nameDate, birthDate, password);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//ユーザ新規登録
	public int newUserRegistration(String loginId, String name, String birthDate, String password) {

		int resultNum = 0;
		Connection conn = null;

		//パスワードつけるメソッドの呼び出し
		String result = invisiblePass(password);

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "INSERT INTO user(login_id, name, birth_date, password,create_date,update_date) VALUES(?, ?, ?, ?, NOW(), NOW())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, result);
			resultNum = pStmt.executeUpdate();

			return resultNum;

		} catch (SQLException e) {
			e.printStackTrace();
			return resultNum;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return resultNum;
				}
			}
		}

	}

	public User userDetails(String id) {

		Connection conn = null;

		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT login_id, name, birth_date, create_date, update_date FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			String registerDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");

			return new User(loginIdData, nameData, birthDateData, registerDateData, updateDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	public User userId(String id) {
		Connection conn = null;

		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");

			return new User(idData, loginIdData, nameData, birthDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	//パスなし
	public int userUpdateWithoutPass(String loginId, String userName, String birthDate) {

		Connection conn = null;
		int resultNum = 0;

		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ

			String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = NOW() WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, loginId);
			resultNum = pStmt.executeUpdate();

			return resultNum;

		} catch (SQLException e) {
			e.printStackTrace();
			return resultNum;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return resultNum;
				}

			}
		}
	}

	//パスワード付き更新
	public int userUpdate(String loginId, String name, String birthDate, String password) {

		Connection conn = null;
		int resultNum = 0;

		String result = invisiblePass(password);


		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ

			String sql = "UPDATE user SET name = ?, birth_date = ?, password = ?, update_date = NOW() WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, result);
			pStmt.setString(4, loginId);
			resultNum = pStmt.executeUpdate();

			return resultNum;

		} catch (SQLException e) {
			e.printStackTrace();
			return resultNum;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return resultNum;
				}

			}
		}
	}

	public User userId2(String id) {

		Connection conn = null;
		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			// 	delete文⇒String sql = "DELETE FROM user WHERE login_id = ?";
			String sql = "SELECT login_id, name, birth_date FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");

			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}

			}
		}
	}

	public int userDelete(String id) {

		Connection conn = null;
		int resultNum = 0;

		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "DELETE FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			resultNum = pStmt.executeUpdate();

			return resultNum;

		} catch (SQLException e) {
			e.printStackTrace();
			return resultNum;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return resultNum;
				}

			}
		}
	}

	public User checkId(String id) {

		Connection conn = null;

		try {
			// データベース接続
			conn = DBManager.getConnection();

			// SELECT文は実行してからコピペ
			String sql = "SELECT*FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}

			}
		}
	}

	//アップデート
	public static String invisiblePass(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			//標準出力
			System.out.println(result);
			return result;

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		}

	}

}
