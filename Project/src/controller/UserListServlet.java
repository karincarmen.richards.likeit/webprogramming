package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる

		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");

		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//ログインIDをセット
		String checkId = userInfo.getLoginId();

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

			if(!checkId.equals("admin")) {
				System.out.println("管理者じゃない");

				//
		        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
		        dispatcher.forward(request, response);
		        return;
			}

			if(checkId.equals("admin")) {
				System.out.println("管理者");


				// ユーザ一覧のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
				dispatcher.forward(request, response);
				return;
			}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//未実装：検索処理全般

		//リクエストパラメータの取得

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String dateStart = request.getParameter("date-start");
		String dateEnd = request.getParameter("date-end");

		UserDao userDao = new UserDao();
		List<User> userFind = userDao.findUserFromList(loginId, name, dateStart, dateEnd);

		//検索失敗
		if(userFind == null) {
			//リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "該当データがありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
			dispatcher.forward(request, response);
			return;
		}


		request.setAttribute("userList", userFind);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_list.jsp");
        dispatcher.forward(request, response);

		//ユーザ一覧サーブレットにリダイレクト
		//response.sendRedirect("LoginServlet");
	}
}