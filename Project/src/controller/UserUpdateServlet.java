package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		if (userInfo == null) {
			response.sendRedirect("UserListServlet");
			return;
		}

		String id = request.getParameter("id");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.userId(id);
		// リクエストスコープにユーザ一idをセット
		request.setAttribute("user", user);

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//空白確認用変数
		String str = "";

		//リクエストパラメータの取得

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");

		System.out.println(loginId);
		System.out.println("パラメータの取得OK");

		if (!password.equals(checkPassword)) {

			request.setAttribute("errMsg", "入力された内容が正しくありません");

			System.out.println("パスワード確認がだめっだった");

			String id = request.getParameter("id");

			// ユーザ一覧情報を取得
			UserDao userDao = new UserDao();
			User user = userDao.userId(id);
			// リクエストスコープにユーザ一idをセット
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//空白確認

		if (name.equals(str) || birthDate.equals(str)) {
			request.setAttribute("errMsg", "入力された内容が正しくありません");

			System.out.println("どこかが空白");

			String id = request.getParameter("id");

			// ユーザ情報を取得
			UserDao userDao = new UserDao();
			User user = userDao.userId(id);
			// リクエストスコープにユーザ一idをセット
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;

		}

		if (password.isEmpty() || checkPassword.isEmpty()) {
			UserDao userDao2 = new UserDao();
			int reg = userDao2.userUpdateWithoutPass(loginId, name, birthDate);

			if (reg == 0) {
				request.setAttribute("errMsg", "入力された内容が正しくありません");

				System.out.println("regが0");

				String id = request.getParameter("id");

				// ユーザ情報を取得
				UserDao userDao = new UserDao();
				User user = userDao.userId(id);
				// リクエストスコープにユーザ一idをセット
				request.setAttribute("user", user);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
				dispatcher.forward(request, response);
				return;
			}
		} else {

			UserDao userDao3 = new UserDao();
			int reg2 = userDao3.userUpdate(loginId, name, birthDate, password);

			if (reg2 == 0) {
				request.setAttribute("errMsg", "入力された内容が正しくありません");

				System.out.println("regが0");

				String id = request.getParameter("id");

				// ユーザ情報を取得
				UserDao userDao = new UserDao();
				User user = userDao.userId(id);
				// リクエストスコープにユーザ一idをセット
				request.setAttribute("user", user);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
				dispatcher.forward(request, response);
				return;
			}
		}
		//ユーザ一覧サーブレットにリダイレクト
		response.sendRedirect("UserListServlet");

	}
}
