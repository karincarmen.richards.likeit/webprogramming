package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserRegistrationServlet
 */
@WebServlet("/UserRegistrationServlet")
public class UserRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserRegistrationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//空白
		String str = "";

		//リクエストパラメータの取得
		String loginId = request.getParameter("loginId");
		String userName = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");


		System.out.println(loginId);
		System.out.println("パラメータの取得OK");



		//空白確認

		if (loginId.equals(str) || userName.equals(str) || birthDate.equals(str) || password.equals(str)) {
			request.setAttribute("errMsg", "入力された内容が正しくありません");

			System.out.println("どこかが空白");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//ID重複確認
		UserDao checkDao = new UserDao();
		User user = checkDao.checkId(loginId);

		if(user!=null) {
			request.setAttribute("errMsg", "入力された内容が正しくありません");

			// jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
			dispatcher.forward(request, response);
			return;
		}



		//パスワードが確認とあっているか
		if (!password.equals(checkPassword)) {

			request.setAttribute("errMsg", "入力された内容が正しくありません");

			System.out.println("if文のエラー");

			// 新規登録のjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
			dispatcher.forward(request, response);
			return;

		}

		UserDao userDao = new UserDao();
		int reg = userDao.newUserRegistration(loginId, userName, birthDate, password);

		if (reg == 0) {
			request.setAttribute("errMsg", "入力された内容が正しくありません");

			System.out.println("regが0");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_registration.jsp");
			dispatcher.forward(request, response);
			return;
		}
		System.out.println("regが１⇒登録された");

		response.sendRedirect("UserListServlet");
	}

}
