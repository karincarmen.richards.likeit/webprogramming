package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User userInfo = (User) session.getAttribute("userInfo");
		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
		}

		String id = request.getParameter("id");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User userId2 = userDao.userId2(id);
		// リクエストスコープにユーザ一idをセット
		request.setAttribute("user", userId2);


		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//リクエストパラメータの取得
		String loginId = request.getParameter("loginId");

		System.out.println(loginId);
		System.out.println("登録されている");

		UserDao userDao = new UserDao();
		int reg = userDao.userDelete(loginId);

		if(reg == 0) {
			request.setAttribute("errMsg", "入力された内容が正しくありません");

			System.out.println("regが0");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_delete.jsp");
			dispatcher.forward(request, response);
		}else {

			System.out.println("regが1");


		//ユーザ一覧サーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
		}
	}

}
